import { Usuario } from './usuario.model ';

export class UsuarioLogin {

  access_token: string;
  token_type: string;
  expires_in: number;
  user: Usuario;


  constructor() {}

  deserialize(input: any) {
    Object.assign(this,input);
    return this;
}
}
