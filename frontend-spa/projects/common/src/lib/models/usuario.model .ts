export class Usuario {

  usuario_id: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  sexo: string;
  fechaNacimiento: string;
  correoElectronico: string;
  ultimoAcceso: string;


  constructor() {}

  deserialize(input: any) {
    Object.assign(this,input);
    return this;
}
}
