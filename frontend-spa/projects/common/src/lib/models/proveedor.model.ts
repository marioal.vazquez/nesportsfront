export class Proveedor {
    proveedor_id: number;
    nombreComercial: string;
    nombreEncargado: string;
    correoElectronico: string;
    telefono: string;
    
    constructor(){ }

    deserialize(input: any) {
        Object.assign(this,input);
        return this;
    }
}