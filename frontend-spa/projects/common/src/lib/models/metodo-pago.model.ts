import { CatTipoPago } from './cat-tipo-pago.model';
import { Usuario } from './usuario.model ';

export class MetodoPago {

  metodoPago_id: number;
  nombre: string;
  numeroTarjeta: string;
  cat_tipo_pago: CatTipoPago;
  usuario: Usuario;
  fechaCreacion: string;
  fechaModificacion: string;
  expiracion: string;


  constructor(){ }

  deserialize(input: any) {
      Object.assign(this,input);
      return this;
  }
}
