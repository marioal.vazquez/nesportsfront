export class Color {

  catColor_id: number;
  nombre: string;
  hexadecimal: string;

  constructor(){ }

    deserialize(input: any) {
        Object.assign(this,input);
        return this;
    }
}
