export class CatTalla {
  catTalla_id: number;
  nombre: string;
  catTipoTalla_id: number;

  constructor(){ }

    deserialize(input: any) {
        Object.assign(this,input);
        return this;
    }
}
