import { Marca } from "./marca.model";
import { Proveedor } from "./proveedor.model";
import { CatDeporte } from './cat-deporte.model';
import { CatGenero } from './cat-genero.model';
import { CategoriaProducto } from './categoria-producto.model';
import { EstadisticaProducto } from "./estadistica-producto.model";
import { ImagenProducto } from './imagen-producto.model';
import { ProductoInventario } from './producto-inventario.model';

export class Producto {
    producto_id: number;
    nombre: string;
    descripcion: string;
    marca_id: number;
    proveedor_id: number;
    precio: number;
    categoriaProducto_id: number;
    subCategoriaProducto_id: number;
    fechaCreacion: string;
    fechaModificacion: string;
    catGenero_id: number;
    catDeporte_id: number;


    marca: Marca;
    proveedor: Proveedor;
    cat_deporte: CatDeporte;
    cat_genero: Array<CatGenero>;
    categoria_producto: CategoriaProducto;
    subcategoria_producto: CategoriaProducto;
    estadistica_productos: Array<EstadisticaProducto>;
    imagenes: Array<ImagenProducto>;
    producto_inventarios: Array<ProductoInventario>;


    constructor(){ }

    deserialize(input: any) {
        Object.assign(this,input);
        return this;
    }
}
