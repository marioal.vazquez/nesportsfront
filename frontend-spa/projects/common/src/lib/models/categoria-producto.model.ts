export class CategoriaProducto {
  categoriaProducto_id: number;
  nombre: string;
  fechaCreacion: string;
  fechaModificacion: string;
  categoriaPadre_id: number;
  descripcion: string;

  CategoriaPadre: CategoriaProducto;

  constructor(){ }

    deserialize(input: any) {
        Object.assign(this,input);
        return this;
    }
}
