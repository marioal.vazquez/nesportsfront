export class CatTipoTalla {

  catTipoTalla_id: number;
  nombre: string;

  constructor(){ }

    deserialize(input: any) {
        Object.assign(this,input);
        return this;
    }
}
