export class ImagenProducto {

  imagenProducto_id: number;
  nombreArchivo: string;
  ruta: string;
  formato: string;
  fechaCreacion: string;
  fechaModificacion: string;
  peso: string;
  producto_id: number;
  catColor_id: number;
  imagen: File;

  constructor(){ }

    deserialize(input: any) {
        Object.assign(this,input);
        return this;
    }
}
