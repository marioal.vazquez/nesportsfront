export class ProductoInventario {

  productoInventario_id: number;
  producto_id: number;
  color_id: number;
  catTalla_id: number;
  cantidad: number;
  fechaCreacion: number;
  fechaModificacion: number;

  constructor(){ }

    deserialize(input: any) {
        Object.assign(this,input);
        return this;
    }
}
