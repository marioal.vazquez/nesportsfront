import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonComponent } from './common.component';
import { ApiService } from './services/api.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [CommonComponent],
  imports: [
    HttpClientModule
  ],
  exports: [CommonComponent]
})
export class CommonModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CommonModule,
      providers: [
        ApiService
      ]
    }
  }
 }
