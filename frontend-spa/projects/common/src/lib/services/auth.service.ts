import { Injectable } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";
import { UsuarioLogin } from '../models/usuario-login.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private jwt: string;

  constructor() { }

  validateToken(): boolean{
    let usuario = new UsuarioLogin().deserialize(
      JSON.parse(
      localStorage.getItem('usuario')
      ));
    if (!usuario) return false;

    const helper = new JwtHelperService();
    const token: string = usuario.access_token;
    const decodedToken = helper.decodeToken(token);
    const expirationDate = helper.getTokenExpirationDate(token);
    const isExpired = helper.isTokenExpired(token);
    return isExpired;
  }

  login(usuario: UsuarioLogin): void{
    localStorage.setItem('usuario', JSON.stringify(usuario));
  }

  logout(): void{
    localStorage.removeItem('usuario');
  }
}
