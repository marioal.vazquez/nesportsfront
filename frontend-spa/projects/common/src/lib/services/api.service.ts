import { environment } from '../../../../nesports-frontend/src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  private endpoint: string;
  private actionUrl: string;
  private headers: any; // Con HttpClient que es la última versión los headers se vuelven inmutables
  private reqMethod: string;
  private reqParams: any;
  private reqbody: any;

  constructor(private http: HttpClient) {
      this.headers = {};
      this.setEndpoint('endpoint');
      this.reqParams = {};
  }

  private getHeaders(){
    const headers = new HttpHeaders(...this.headers, {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'X-Requested-By':'SISI Front'
    });
    return headers;
  }

  public setEndpoint(key: string){
    const check = Object.keys(environment).find(item => item === key);
    this.endpoint = (check !== undefined) ? environment[check] : environment['endpoint'];
    return this;
  }

  public setActionUrl(actionUrl: string) {
      this.actionUrl = actionUrl;
      return this;
  }

  public setHeaders(otherHeaders: any) {
      this.headers = otherHeaders;
      return this;
  }

  public setReqMethod(reqMethod: string) {
      this.reqMethod = reqMethod;
      return this;
  }

  public setReqParams(reqParams: any) {
      this.reqParams = reqParams;
      return this;
  }

  public setBody(reqbody: any) {
      this.reqbody = reqbody;
      return this;
  }

  encodeQueryData(data: any) {
      const ret: any[] = [];
      for (let d in data) {
          ret.push(d + '=' + data[d]);
      }
      return ret.join('&');
  }

  public getActionUrl(actionUrl: string, reqParams: any) {
      const queryString = {};
      const bodyKey = Object.keys(reqParams);
      if (bodyKey.length > 0) {
          bodyKey.forEach(key => {
              if (actionUrl.indexOf(key) > -1) {

                  actionUrl = actionUrl.replace(':' + key, reqParams[key]);
              } else {
                  queryString[key] = reqParams[key];
              }
          });
      }
      const encodedUrl = this.encodeQueryData(queryString);
      if (encodedUrl.length > 0) {
          return actionUrl + '?' + this.encodeQueryData(queryString);
      } else {
          return actionUrl;
      }
  }

  public getEndpoint() {
      const newAction = this.getActionUrl(this.actionUrl, this.reqParams);
      console.log(this.endpoint + newAction);
      return this.endpoint + newAction;
  }

  public getUrlServer() {
    const newAction = this.getActionUrl(this.actionUrl, this.reqParams);
    return this.endpoint;
  }

  private handleError(error: HttpErrorResponse) {
      if ([401, 403, 400, 404, 500].includes(error.status)) {
          console.log('Error de API, redireccionar?');
          return Observable.throw(error.error || 'Error de Api');
      }
      console.log(error);
      return Observable.throw(error.message || 'Server error');
  }

  public makeCallFile(){
    return this.http.post(this.getEndpoint(), this.reqbody,
    {
        responseType: 'arraybuffer',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/pdf'
        }
      })
      .catch(this.handleError.bind(this));
}

  public makeCall(): Observable<{}> {
      this.setCSV();
      let url = this.getEndpoint();
      this.reqParams = {};
      switch (this.reqMethod) {
          case 'GET':
              return this.http.get(url,
                  { headers: this.getHeaders() })
                  .catch(this.handleError.bind(this));
          case 'POST':
              return this.http.post(url, this.reqbody, { headers: this.getHeaders() })
                  .catch(this.handleError.bind(this));
          case 'PUT':
              return this.http.put(url, this.reqbody, { headers: this.getHeaders() })
                  .catch(this.handleError.bind(this));
          case 'DELETE':
              return this.http.delete(url, { headers: this.getHeaders() })
              .catch(this.handleError.bind(this));
      }

  }
  setCSV(){
      localStorage.removeItem('csv');
      // let modulo: any = JSON.parse(localStorage.getItem('moduloAux'));
      // let submodulo: any = JSON.parse(localStorage.getItem('subModuloAux'));
      // let proceso: any = JSON.parse(localStorage.getItem('activeProceso'));
  }
  encodeString(reqParams){
      const queryString = {};
      const bodyKey = Object.keys(reqParams);
      if (bodyKey.length > 0) {
          bodyKey.forEach(key => {
              queryString[key] = reqParams[key];
          });
      }
      const encodedUrl = this.encodeQueryData(queryString);
      if (encodedUrl.length > 0) {
          return encodedUrl;
      } else {
          return '';
      }
  }

  public download(){
    const httpOptions = {
        responseType: 'blob' as 'json',
        headers: this.getHeaders()
      };
    return this.http.post(this.getEndpoint(), this.reqbody, httpOptions).catch(this.handleError.bind(this));
  }
  public downloadLocal(){
    const httpOptions = {
        responseType: 'blob' as 'json',
        headers: this.getHeaders(),
        observe: 'response' as 'body'
      };
    return this.http.post(this.getEndpoint(), this.reqbody, httpOptions).catch(this.handleError.bind(this));
  }
}
