import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from './login/login.component';

const ROUTES: Routes = [
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'home',
        component: HomeComponent
    }  
];

export const APP_ROUTING = RouterModule.forRoot(ROUTES, {useHash: true});

