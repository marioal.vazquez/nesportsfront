import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from "@angular/router";
import { Subscription } from 'rxjs';
import { ApiService } from 'projects/common/src/lib/services/api.service';
import { AuthService } from "projects/common/src/lib/services/auth.service";
import { UsuarioLogin } from 'projects/common/src/lib/models/usuario-login.model';


@Component({
  selector: 'app-login-boxed',
  templateUrl: './login-boxed.component.html',
  styles: []
})
export class LoginBoxedComponent implements OnInit {

  public loginForm: FormGroup;

  loginSubs = new Subscription();
  usuario: UsuarioLogin;
  errorMsg: string;

  constructor(private apiService: ApiService, private authService: AuthService, private router: Router) {
    this.loginForm = new FormGroup({
      correoElectronico: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {

  }

  public iniciarSesion(){
    //this.usuario = new UsuarioLogin().deserialize(this.loginForm.value);
    this.loginSubs = this.apiService
      .setEndpoint('backend')
      .setActionUrl('login')
      .setReqMethod('POST')
      .setBody(this.loginForm.value)
      .makeCall()
      .subscribe((res: any) => {
        this.usuario = new UsuarioLogin().deserialize(res);
        if (!this.usuario) {
          this.errorMsg = 'Usuario o contraseña no válidos';
          return;
        }
        this.authService.login(this.usuario);
        return this.router.navigate(['/']);
      }, (error: any) =>{
        this.errorMsg = 'Algo no salió bien, intenta más tarde';
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  unsubscribe(){
    this.loginSubs.unsubscribe();
  }

}
