import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from 'rxjs';

// Modelos
import { Marca } from "projects/common/src/lib/models/marca.model";

// Servicios
import { ApiService } from "projects/common/src/lib/services/api.service";


@Component({
  selector: 'app-marca',
  templateUrl: './marca.component.html',
  styleUrls: ['./marca.component.scss']
})
export class MarcaComponent implements OnInit {

  heading = 'Administrador de Marcas';
  subheading = 'Crea, edita y elimina las marcas de la tienda.';
  icon = 'pe-7s-graph text-success';

  agregarEditarActive = false;
  sinMarcas = false;

  btnmsg = '';

  marcaForm = new FormGroup({
    marca_id: new FormControl(null, []),
    nombre: new FormControl(null, [Validators.required])
  });

  agregarMarcaSubs = new Subscription();
  getMarcasSubs = new Subscription();
  eliminarMarcaSubs = new Subscription();
  marcasSubs = new Subscription();

  marcaSelected: Marca;
  marcas = new Array<Marca>();

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getMarcas();
  }

  getMarcas(){
    this.getMarcasSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('marcas')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      if (res.data.length == 0) {
        return this.sinMarcas = true;
      }
      this.marcas = res.data;

      this.getMarcasSubs.unsubscribe();
    }, (error: any) =>{
      this.getMarcasSubs.unsubscribe();
    })
  }

  agregarEditarMarca(){
    this.agregarMarcaSubs = this.apiService
    .setEndpoint('backend')
    .setActionUrl('marcas')
    .setReqMethod('POST')
    .setBody(this.marcaForm.value)
    .makeCall()
    .subscribe((res: any) => {

      this.marcas = res.data;
    }, (error: any) =>{
      this.agregarMarcaSubs.unsubscribe();
    })
  }

  editarMarca(marca: Marca){
    this.btnmsg = 'Actualizar';
    this.agregarEditarActive = true;
    this.marcaSelected = marca;
    this.marcaForm.patchValue(marca);
  }

  eliminarMarca(marca_id: number){
    this.eliminarMarcaSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('marcas/eliminar')
    .setReqMethod('POST')
    .setBody({
      marca_id: marca_id
    })
    .makeCall()
    .subscribe((res:any) =>{

      this.marcas = res.data;
      this.eliminarMarcaSubs.unsubscribe();
    }, (error: any) =>{

      this.eliminarMarcaSubs.unsubscribe();
    })
  }

  agregarMarca(){
    this.marcaForm.reset();
    this.agregarEditarActive = true;
    this.btnmsg = 'Agregar';
  }

  unsubscribe(){
    this.marcasSubs.unsubscribe();
    this.getMarcasSubs.unsubscribe();
    this.agregarMarcaSubs.unsubscribe();
    this.eliminarMarcaSubs.unsubscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

}
