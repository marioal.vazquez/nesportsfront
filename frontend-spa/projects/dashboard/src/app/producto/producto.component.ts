import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from "@angular/forms";
import { Subscription } from 'rxjs';
import { Producto } from 'projects/common/src/lib/models/producto.model';
import { Marca } from "projects/common/src/lib/models/marca.model";
import { Proveedor } from "projects/common/src/lib/models/proveedor.model";
import { ApiService } from 'projects/common/src/lib/services/api.service';
import { CategoriaProducto } from 'projects/common/src/lib/models/categoria-producto.model';
import { CatGenero } from "projects/common/src/lib/models/cat-genero.model";
import { CatDeporte } from "projects/common/src/lib/models/cat-deporte.model";
import { Color } from "projects/common/src/lib/models/color.model";
import { CatTalla } from "projects/common/src/lib/models/cat-talla.model";
import { CatTipoTalla } from "projects/common/src/lib/models/cat-tipo-talla.model";
import { ImagenProducto } from 'projects/common/src/lib/models/imagen-producto.model';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})

export class ProductoComponent implements OnInit {

  heading = 'Administrador de Productos';
  subheading = 'Crea, edita y elimina los productos de la tienda.';
  icon = 'pe-7s-graph text-success';

  agregarEditarActive = false;
  sinProductos = false;

  btnmsg = '';

  get producto_inventarios() { return this.productoForm.get('producto_inventarios') as FormArray }


  agregarProductoSubs = new Subscription();
  getProductosSubs = new Subscription();
  eliminarProductoSubs = new Subscription();

  // Catalogos
  proveedoresSubs = new Subscription();
  marcasSubs = new Subscription();
  categoriasSubs = new Subscription();
  catGeneroSubs = new Subscription();
  catDeporteSubs = new Subscription();
  coloresSubs = new Subscription();
  catTallasSubs = new Subscription();
  catTipoTallasSubs = new Subscription();
  guardarImagenesSubs = new Subscription();

  productoSelected: Producto;
  productos = new Array<Producto>();

  producto_idGuardado: number;

  //Catalogos
  marcas = new Array<Marca>();
  proveedores = new Array<Proveedor>();
  public categorias = new Array<CategoriaProducto>();
  public generos = new Array<CatGenero>();
  public deportes = new Array<CatDeporte>();
  public colores = new Array<Color>();
  public catTipoTallas = new Array<CatTipoTalla>();
  public catTallas = new Array<CatTalla>();
  public catTallasFiltradas = new Array<CatTalla>();


  productoForm: FormGroup;

  imagenesProducto = new Array<ImagenProducto>();

  constructor(private apiService: ApiService, private fb: FormBuilder) {
      this.productoForm = this.fb.group({
        producto_id: this.fb.control(null, []),
        nombre: this.fb.control(null, [Validators.required]),
        descripcion: this.fb.control(null, [Validators.required]),
        marca_id: this.fb.control(null, [Validators.required]),
        proveedor_id: this.fb.control(null, [Validators.required]),
        precio: this.fb.control(null, [Validators.required]),
        categoriaProducto_id: this.fb.control(null, [Validators.required]),
        subCategoriaProducto_id: this.fb.control(null, [Validators.required]),
        fechaCreacion: this.fb.control(null, []),
        fechaModificacion: this.fb.control(null, []),
        catGenero_id: this.fb.control(null, [Validators.required]),
        catDeporte_id: this.fb.control(null, [Validators.required]),
        producto_inventarios: this.fb.array([])
      });
   }

  ngOnInit() {
    this.getProductos();
    this.getMarcas();
    this.getProveedores();
    this.getGeneros();
    this.getDeportes();
    this.getCategoriasAll();
    this.getColores();
    this.getTallasAll();
    this.getTipoTallas();
  }

  agregarEditarProducto(){
    this.agregarProductoSubs = this.apiService
      .setEndpoint('backend')
      .setActionUrl('productos')
      .setReqMethod('POST')
      .setBody(this.productoForm.value)
      .makeCall()
      .subscribe((res: any) => {
        console.log('====================================');
        console.log(res);
        console.log('====================================');
        this.productos = res.data.data;
        this.producto_idGuardado = res.data.producto_id;
        this.imagenesProducto.forEach((item, index) =>{
          this.guardarImagenes(item, this.producto_idGuardado);
        })
      }, (error: any) =>{
        this.agregarProductoSubs.unsubscribe();
      })
  }

  editarProducto(producto: Producto){
    this.btnmsg = 'Actualizar';
    this.agregarEditarActive = true;
    this.productoSelected = producto;
    this.productoForm.patchValue(producto);
  }

  eliminarProducto(producto_id: number){
    this.eliminarProductoSubs = this.apiService.setEndpoint('backend')
      .setActionUrl('productos/eliminar')
      .setReqMethod('POST')
      .setReqParams({
        producto_id: producto_id
      })
      .makeCall()
      .subscribe((res:any) =>{

        this.productos = res.data;
        this.eliminarProductoSubs.unsubscribe();
      }, (error: any) =>{

        this.eliminarProductoSubs.unsubscribe();
      })
  }

  agregarProducto(){
    this.productoForm.reset();
    this.agregarEditarActive = true;
    this.btnmsg = 'Agregar';
  }

  getProductos(){
    this.getProductosSubs = this.apiService.setEndpoint('backend')
      .setActionUrl('productos')
      .setReqMethod('GET')
      .makeCall()
      .subscribe((res:any) =>{
        if (res.data.length == 0) {
          return this.sinProductos = true;
        }
        this.productos = res.data;

        //this.getProductosSubs.unsubscribe();
      }, (error: any) =>{
        this.getProductosSubs.unsubscribe();
      })
  }

  getMarcas(){
    this.marcasSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('marcas')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      this.marcas = res.data;
      this.marcasSubs.unsubscribe();
    }, (error: any) =>{
      this.marcasSubs.unsubscribe();
    })
  }

  getProveedores(){
    this.proveedoresSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('proveedores')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      this.proveedores = res.data;
      this.proveedoresSubs.unsubscribe();
    }, (error: any) =>{
      this.proveedoresSubs.unsubscribe();
    });
  }

  getGeneros(){
    this.catGeneroSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('cat_generos')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      this.generos = res.data;
      this.catGeneroSubs.unsubscribe();
    }, (error: any) =>{
      this.catGeneroSubs.unsubscribe();
    });
  }

  getDeportes(){
    this.catDeporteSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('cat_deportes')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      this.deportes = res.data;
      this.catDeporteSubs.unsubscribe();
    }, (error: any) =>{
      this.catDeporteSubs.unsubscribe();
    });
  }

  getCategoriasAll(){
    this.categoriasSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('categoria_productos')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      this.categorias = res.data;
      this.categoriasSubs.unsubscribe();
    }, (error: any) =>{
      this.categoriasSubs.unsubscribe();
    });
  }

  getColores(){
    this.coloresSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('cat_colores')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      this.colores = res.data;
      this.coloresSubs.unsubscribe();
    }, (error: any) =>{
      this.coloresSubs.unsubscribe();
    });
  }

  getTallasAll(){
    this.catTallasSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('cat_tallas')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      this.catTallas = res.data;
      this.catTallasSubs.unsubscribe();
    }, (error: any) =>{
      this.catTallasSubs.unsubscribe();
    });
  }

  getTipoTallas(){
    this.catTipoTallasSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('cat_tipos_talla')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      this.catTipoTallas = res.data;
      this.catTipoTallasSubs.unsubscribe();
    }, (error: any) =>{
      this.catTipoTallasSubs.unsubscribe();
    });
  }

  getTallas(i: number) {
    let catTipoTalla_id = Number(this.producto_inventarios.controls[i].value.catTipoTalla_id);
    if (!catTipoTalla_id) return this.catTallasFiltradas = new Array<CatTalla>();
    this.catTallasFiltradas = this.catTallas.filter(ct => ct.catTipoTalla_id == catTipoTalla_id);
  }

  getCategorias(): Array<CategoriaProducto>{
    return this.categorias.filter(c => c.categoriaPadre_id == null);
  }

  getSubCategorias(): Array<CategoriaProducto>{
    let categoriaPadre_id = this.productoForm.value.categoriaProducto_id;
    if (!categoriaPadre_id) return new Array<CategoriaProducto>();

    return this.categorias.filter(c => c.categoriaPadre_id == categoriaPadre_id);

  }

  addInventarioForm(){
    this.producto_inventarios.push(this.fb.group({
      productoInventario_id: this.fb.control(null, []),
      producto_id: this.fb.control(null, []),
      catColor_id: this.fb.control(null, [Validators.required]),
      catTalla_id: this.fb.control(null, [Validators.required]),
      catTipoTalla_id: this.fb.control(null, [Validators.required]),
      cantidad: this.fb.control(null, [Validators.required]),
      fechaCreacion: this.fb.control(null, []),
      fechaModificacion: this.fb.control(null, []),
    }));
  }

  removeInventarioForm(index: number){
    this.producto_inventarios.removeAt(index);
  }

  uploadFiles(index: number, e: any){
    let catColor_id = Number(this.producto_inventarios.controls[index].value.catColor_id);
    for (let index = 0; index < e.target.files.length; index++) {
      this.imagenesProducto.push(
        new ImagenProducto().deserialize({
          nombreArchivo: e.target.files[index].name,
          formato: e.target.files[index].type,
          peso: `${Math.round(e.target.files[index].size / 1024)} kBs`,
          catColor_id: catColor_id,
          imagen: e.target.files.item(index)
        })
      );
    }
    console.log({
      'archivos': this.imagenesProducto
    });
  }

  guardarImagenes(_imagen: ImagenProducto, producto_id: number){

    let formData = new FormData();
    formData.append('imagen', _imagen.imagen);
    formData.append('informacion', JSON.stringify({
      formato: _imagen.formato,
      nombreArchivo: _imagen.nombreArchivo,
      peso: _imagen.peso,
      catColor_id: _imagen.catColor_id,
      producto_id: producto_id
    }));



    this.guardarImagenesSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('productos/guardarImagen')
    .setReqMethod('POST')
    .setBody(formData)
    .setHeaders({})
    .makeCall()
    .subscribe((res:any) =>{
      console.assert('Archivo guardado');
      this.guardarImagenesSubs.unsubscribe();
    }, (error: any) =>{
      console.error('Archivo fallido');
      this.guardarImagenesSubs.unsubscribe();
    });
  }

  unsubscribe(){
    this.marcasSubs.unsubscribe();
    this.proveedoresSubs.unsubscribe();
    this.getProductosSubs.unsubscribe();
    this.agregarProductoSubs.unsubscribe();
    this.eliminarProductoSubs.unsubscribe();
    this.catGeneroSubs.unsubscribe();
    this.catDeporteSubs.unsubscribe();
    this.coloresSubs.unsubscribe();
    this.guardarImagenesSubs.unsubscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }
}
