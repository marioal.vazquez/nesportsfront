import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from 'rxjs';

// Modelos
import { Proveedor } from "projects/common/src/lib/models/proveedor.model";

// Servicios
import { ApiService } from "projects/common/src/lib/services/api.service";

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.scss']
})
export class ProveedorComponent implements OnInit {


  heading = 'Administrador de Proveedores';
  subheading = 'Crea, edita y elimina tus proveedores.';
  icon = 'pe-7s-graph text-success';

  agregarEditarActive = false;
  sinProveedores = false;

  btnmsg = '';

  proveedorForm = new FormGroup({
    proveedor_id: new FormControl(null, []),
    nombreComercial: new FormControl(null, [Validators.required]),
    nombreEncargado: new FormControl(null, [Validators.required]),
    correoElectronico: new FormControl(null, [Validators.required]),
    telefono: new FormControl(null, [Validators.required])
  });

  agregarProveedorSubs = new Subscription();
  getProveedoresSubs = new Subscription();
  eliminarProveedorSubs = new Subscription();
  proveedoresSubs = new Subscription();

  proveedoresSelected: Proveedor;
  proveedores = new Array<Proveedor>();


  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getProveedores();
  }

  getProveedores(){
    this.getProveedoresSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('proveedores')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      if (res.data.length == 0) {
        return this.sinProveedores = true;
      }
      this.proveedores = res.data;

      this.getProveedoresSubs.unsubscribe();
    }, (error: any) =>{
      this.getProveedoresSubs.unsubscribe();
    })
  }

  agregarEditarProveedor(){
    this.agregarProveedorSubs = this.apiService
    .setEndpoint('backend')
    .setActionUrl('proveedores')
    .setReqMethod('POST')
    .setBody(this.proveedorForm.value)
    .makeCall()
    .subscribe((res: any) => {

      this.proveedores = res.data;
    }, (error: any) =>{
      this.agregarProveedorSubs.unsubscribe();
    })
  }

  editarProveedor(proveedor: Proveedor){
    this.btnmsg = 'Actualizar';
    this.agregarEditarActive = true;
    this.proveedoresSelected = proveedor;
    this.proveedorForm.patchValue(proveedor);
  }

  eliminarProveedor(proveedor_id: number){
    this.eliminarProveedorSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('proveedores/eliminar')
    .setReqMethod('POST')
    .setBody({
      proveedor_id: proveedor_id
    })
    .makeCall()
    .subscribe((res:any) =>{

      this.proveedores = res.data;
      this.eliminarProveedorSubs.unsubscribe();
    }, (error: any) =>{

      this.eliminarProveedorSubs.unsubscribe();
    })
  }

  agregarMarca(){
    this.proveedorForm.reset();
    this.agregarEditarActive = true;
    this.btnmsg = 'Agregar';
  }

  unsubscribe(){
    this.proveedoresSubs.unsubscribe();
    this.getProveedoresSubs.unsubscribe();
    this.agregarProveedorSubs.unsubscribe();
    this.eliminarProveedorSubs.unsubscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }




}
