import {Component, OnInit} from '@angular/core';
import {ThemeOptions} from '../../../../../theme-options';

// Services
import { AuthService } from 'projects/common/src/lib/services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-box',
  templateUrl: './user-box.component.html',
})
export class UserBoxComponent implements OnInit {

  constructor(public globals: ThemeOptions, private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  cerrarSesion(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
