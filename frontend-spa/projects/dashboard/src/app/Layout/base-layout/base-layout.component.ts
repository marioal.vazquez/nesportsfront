import {Component} from '@angular/core';
import {select} from '@angular-redux/store';
import { Router } from "@angular/router";
import {Observable} from 'rxjs';
import {ConfigActions} from '../../ThemeOptions/store/config.actions';
import {ThemeOptions} from '../../theme-options';
import {animate, query, style, transition, trigger} from '@angular/animations';
import { log } from 'util';

// Services

import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthService } from 'projects/common/src/lib/services/auth.service';

@Component({
  selector: 'app-base-layout',
  templateUrl: './base-layout.component.html',
  animations: [

    trigger('architectUIAnimation', [
      transition('* <=> *', [
        query(':enter, :leave', [
          style({
            opacity: 0,
            display: 'flex',
            flex: '1',
            transform: 'translateY(-20px)',
            flexDirection: 'column'

          }),
        ]),
        query(':enter', [
          animate('600ms ease', style({opacity: 1, transform: 'translateY(0)'})),
        ]),

        query(':leave', [
          animate('600ms ease', style({opacity: 0, transform: 'translateY(-20px)'})),
         ], { optional: true })
      ]),
    ])
  ]
})

export class BaseLayoutComponent {

  @select('config') public config$: Observable<any>;

  constructor(public globals: ThemeOptions, public configActions: ConfigActions, private jwtHelper: JwtHelperService,
    private authService: AuthService, private router: Router) {
  }

  toggleSidebarMobile() {
    this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
  }

  ngOnInit(): void {
    if (this.authService.validateToken()) {
      console.warn({
        seguridad: 'Acceso denegado, bitch'
      });
      this.router.navigate(['/login']);
    }
  }
}



