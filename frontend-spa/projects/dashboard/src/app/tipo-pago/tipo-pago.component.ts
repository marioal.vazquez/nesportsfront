import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from 'rxjs';
import { Producto } from 'projects/common/src/lib/models/producto.model';
import { Marca } from "projects/common/src/lib/models/marca.model";
import { Proveedor } from "projects/common/src/lib/models/proveedor.model";
import { ApiService } from 'projects/common/src/lib/services/api.service';
import { Usuario } from 'projects/common/src/lib/models/usuario.model ';
import { CatTipoPago } from 'projects/common/src/lib/models/cat-tipo-pago.model';
import { MetodoPago } from 'projects/common/src/lib/models/metodo-pago.model';

@Component({
  selector: 'app-tipo-pago',
  templateUrl: './tipo-pago.component.html',
  styleUrls: ['./tipo-pago.component.scss']
})
export class TipoPagoComponent implements OnInit {

  heading = 'Administrador Métodos de Pago';
  subheading = 'Gestión de métodos de pago de usuarios.';
  icon = 'pe-7s-graph text-success';

  agregarEditarActive = false;
  sinMetodos = false;

  btnmsg = '';

  metodoPagoForm = new FormGroup({
    metodoPago_id: new FormControl(null, []),
    nombre: new FormControl(null, [Validators.required]),
    numeroTarjeta: new FormControl(null, [Validators.required]),
    catTipoPago_id: new FormControl(null, [Validators.required]),
    usuario_id: new FormControl(null, [Validators.required]),
    fechaModificacion: new FormControl(null, []),
    fechaCreacion: new FormControl(null, []),
    expiracion: new FormControl(null, [Validators.required])
  });

  agregarMetodoPagoSubs = new Subscription();
  getMetodoPagosSubs = new Subscription();
  eliminarMetodoPagoSubs = new Subscription();
  usuariosSubs = new Subscription();
  catTiposPagoSubs = new Subscription();

  metodoPagoSelected: MetodoPago;
  metodosPago = new Array<MetodoPago>();
  usuarios = new Array<Usuario>();
  catTiposPago = new Array<CatTipoPago>();


  constructor(private apiService: ApiService) { }

  ngOnInit() {

    this.getUsuarios();
    this.getCatTiposPago();
    this.getMetodosPago();
  }

  agregarEditarMetodoPago(){
    let metodoPagoTemp = new MetodoPago().deserialize(this.metodoPagoForm.value);
    let numTarjetaTemp = metodoPagoTemp.numeroTarjeta;
    let toSlice = numTarjetaTemp.slice(0,12);
    metodoPagoTemp.numeroTarjeta = metodoPagoTemp.numeroTarjeta.replace(toSlice, '************');

    this.agregarMetodoPagoSubs = this.apiService
      .setEndpoint('backend')
      .setActionUrl('metodos_pago')
      .setReqMethod('POST')
      .setBody(metodoPagoTemp)
      .makeCall()
      .subscribe((res: any) => {

        this.metodosPago = res.data;
      }, (error: any) =>{
        this.agregarMetodoPagoSubs.unsubscribe();
      })
  }

  editarMetodoPago(metodoPago: MetodoPago){
    this.btnmsg = 'Actualizar';
    this.agregarEditarActive = true;
    this.metodoPagoSelected = metodoPago;
    this.metodoPagoForm.patchValue(this.metodoPagoSelected);
  }

  eliminarMetodoPago(metodoPago_id: number){
    this.eliminarMetodoPagoSubs = this.apiService.setEndpoint('backend')
      .setActionUrl('metodos_pago/eliminar')
      .setReqMethod('POST')
      .setReqParams({
        metodoPago_id: metodoPago_id
      })
      .makeCall()
      .subscribe((res:any) =>{

        this.metodosPago = res.data;
        this.eliminarMetodoPagoSubs.unsubscribe();
      }, (error: any) =>{

        this.eliminarMetodoPagoSubs.unsubscribe();
      })
  }

  agregarMetodoPago(){
    this.metodoPagoForm.reset();
    this.agregarEditarActive = true;
    this.btnmsg = 'Agregar';
  }

  getMetodosPago(){
    this.getMetodoPagosSubs = this.apiService.setEndpoint('backend')
      .setActionUrl('metodos_pago')
      .setReqMethod('GET')
      .makeCall()
      .subscribe((res:any) =>{
        if (res.data.length == 0) {
          return this.sinMetodos = true;
        }
        this.metodosPago = res.data;


        this.getMetodoPagosSubs.unsubscribe();
      }, (error: any) =>{
        this.getMetodoPagosSubs.unsubscribe();
      })
  }

  getUsuarios(){
    this.usuariosSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('usuarios')
    .setReqMethod('GET')
    .setReqParams({})
    .makeCall()
    .subscribe((res:any) =>{
      this.usuarios = res.data;
      this.usuariosSubs.unsubscribe();
    }, (error: any) =>{
      this.usuariosSubs.unsubscribe();
    })
  }

  getCatTiposPago(){
    this.catTiposPagoSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('cat_tipos_pago')
    .setReqMethod('GET')
    .makeCall()
    .subscribe((res:any) =>{
      this.catTiposPago = res.data;
      this.catTiposPagoSubs.unsubscribe();
    }, (error: any) =>{
      this.catTiposPagoSubs.unsubscribe();
    })
  }

  unsubscribe(){
    this.getMetodoPagosSubs.unsubscribe();
    this.usuariosSubs.unsubscribe();
    this.agregarMetodoPagoSubs.unsubscribe();
    this.catTiposPagoSubs.unsubscribe();
    this.eliminarMetodoPagoSubs.unsubscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }
}
