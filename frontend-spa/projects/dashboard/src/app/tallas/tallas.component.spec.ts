import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TallasComponent } from './tallas.component';

describe('TallasComponent', () => {
  let component: TallasComponent;
  let fixture: ComponentFixture<TallasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TallasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TallasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
