import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../../../common/src/lib/services/api.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  title = 'dashboard';
  loginSubs = new Subscription();
  loginError = false;
  errorMsg = '';

  loginForm = new FormGroup({
    correoElectronico: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required])
  });

  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit() {
    
  }

  login(){
    event.preventDefault();
    this.loginSubs = this.apiService.setEndpoint('backend')
    .setActionUrl('usuarios/login')
    .setReqMethod('POST')
    .setBody(this.loginForm.value)
    .makeCall()
    .subscribe((res: any) =>{
      this.loginSubs.unsubscribe();
      this.router.navigate(['home']);
    }, (error: any) =>{
      this.loginError = true;
      this.errorMsg = error.message;
      this.loginSubs.unsubscribe();
    });
  }
}
