import { Component, OnInit, ElementRef } from '@angular/core';
import { OwlOptions } from "ngx-owl-carousel-o";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  customOptions: OwlOptions = {
    autoplay: true,
             loop: true,
             items: 1,
             margin: 30,
             stagePadding: 0,
             nav: false,
             dots: true,
             navText: ['<span class="ion-ios-arrow-back">', '<span class="ion-ios-arrow-forward">'],
             responsive: {
                 0: {
                     items: 1
                 },
                 600: {
                     items: 2
                 },
                 1000: {
                     items: 3
                 }
             }
  }

  constructor(private el: ElementRef) { }

  ngOnInit() {
    this.styles();
  }



  styles(){
    setTimeout(() => {
      let loader = this.el.nativeElement.querySelector('#ftco-loader');
      loader.classList.remove('show');
    }, 1000);
  }
}
